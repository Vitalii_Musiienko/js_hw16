let userNumber;

while (!isNumberValid(userNumber)) {
    userNumber = prompt('Please, enter a number', [userNumber]);
}

function isNumberValid(x) {
    if (typeof(+x) === 'number' && !Number.isNaN(+x) && x.length !== 0 && x[0] !== ' ') {
        return true;
    } else {
        return false;
    }
}

function fib(f0, f1, n) {
    if (n == 0) {
        return f0;
    }
    if (n == 1) {
        return f1;
    }
    if (n >= 0) {
        if (n == 2) {
            return f0 + f1;
        } else {
            return fib(f0, f1, n - 1) + fib(f0, f1, n - 2);
        }
        } else {
        if (n == -1) {
            return f0 - f1;
        } else {
            return fib(f0, f1, Number(n) + 1) - (fib(f0, f1, Number(n) + 2) * -1);
        }
    }
    
}

console.log('f0 = 4; f1 = 9; n = ' + userNumber + '; _____ f' + userNumber + ' =', fib(4, 9, userNumber));

// console.group('n > 0    Перевірка' );
// console.log('3 - ', fib(4, 9, 3));
// console.log('2 - ', fib(4, 9, 2));
// console.log('1 - ', fib(4, 9, 1));
// console.log('0 - ', fib(4, 9, 0));
// console.groupEnd();
// console.group('n < 0    Перевірка');
// console.log('-1 - ', fib(4, 9, -1));
// console.log('-2 - ', fib(4, 9, -2));
// console.log('-3 - ', fib(4, 9, -3));
// console.log('-4 - ', fib(4, 9, -4));
// console.log('-5 - ', fib(4, 9, -5));
// console.log('-6 - ', fib(4, 9, -6));
// console.groupEnd();

// console.time('Time for operation with f0 = 4; f1 = 9; n = 40');
// console.log('40 - ', fib(4, 9, 40));
// console.timeEnd('Time for operation with f0 = 4; f1 = 9; n = 40');